import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import 'firebase/firestore';
 
/**
 * Generated class for the UpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {
  item
  items: any=[]
  authors: any[]
  titles: any[]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
 this.updateData();
  }
 
  ionViewDidLoad() {
    
  }
  updateData() {
    console.log(this.navParams.get('data'));
    this.item = this.navParams.get('data');
    let db = firebase.firestore();
    db.collection("songs").get().then(snap =>{
      snap.docs.forEach(doc=>{
        this.items.push(doc.id);
      })
      console.log(this.items[this.item]);
      db.collection("songs").doc(this.items[this.item]).set({
        author: this.authors,
        title: this.titles
      }).then(function() {
        console.log("Successfully updated");
      }).catch(function(error) {
        console.log("Error while updating");
        console.log(error);
      })
    })
 
  }
}
