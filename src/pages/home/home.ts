import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddPage } from '../add/add';
import { UpdatePage } from '../update/update';
import firebase from 'firebase';
import 'firebase/firestore';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	titles
	item
	items: any=[]
	catchTitle: any=[]
	ind 
	passMe
  constructor(public navCtrl: NavController) {
  this.displayData();
  }

goToOtherPage() {
	this.navCtrl.push(AddPage);
}

goToOtherPage2(i) {
this.passMe = i;
this.navCtrl.push(UpdatePage, {
	data: this.passMe
});
	
}

displayData(){
	let db = firebase.firestore();
	db.collection("songs").get().then(snap => {
		snap.docs.forEach(doc=>{
			this.catchTitle.push(doc.data().title);
			this.titles = this.catchTitle;
			console.log(this.titles);
		})
	})
}

deleteItem(i){
	this.ind = i;
	console.log(this.ind);
	let db = firebase.firestore();
	db.collection("songs").get().then(snap=>{
		snap.docs.forEach(doc=>{
			this.items.push(doc.id);
		})
		console.log(this.items[this.ind]);
		db.collection("songs").doc(this.items[this.ind]).delete();
		})
}

}
