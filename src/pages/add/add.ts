import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import 'firebase/firestore';

/**
 * Generated class for the AddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {

	authors: any[]
	titles: any[]
	i
	name = "asd"
	caps : any[]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
   this.addData();
  }

  ionViewDidLoad(caps) {
  let db = firebase.firestore();
  	db.collection("songs").get().then(snap => {
  		snap.docs.forEach(doc=>{
  			if(doc.data().author == this.name){
  				console.log(doc.id);
  			}
  		})
  	})
  }

  addData(){
  	let db = firebase.firestore();
  		db.collection("songs").get().then(snap =>{
  			this.i = snap.size;
  			this.i = this.i+1;
  			db.collection("songs").add({
  				author: this.authors,
  				title: this.titles
  			}).then((data)=>{
  				console.log(data)
  			}).catch((error)=>{
  				console.log(error)
  			})
  		})
  }

}
